package com.soma.core.repositories.jpa;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.repositories.CourseRepo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
@Repository
public class JpaCourseRepo implements CourseRepo
{
    //@PersistenceContext
    private EntityManager em;

    @Override
    public Course createCourse(Course data)
    {
        em.persist(data);
        return data;
    }

    @Override
    public List<Course> findAllCourses()
    {
        Query query = em.createQuery("SELECT c from Course c");
        return query.getResultList();
    }

    @Override
    public Course findCourse(Long id) {
        return em.find(Course.class, id);
    }

    @Override
    public Course findCourseByTitle(String title)
    {
        Query query = em.createQuery("SELECT c FROM Course c where c.title=?1");
        query.setParameter(1, title);
        List<Course> courses = query.getResultList();

        if(courses.isEmpty())
        {
            return null;
        }
        else
        {
            return courses.get(0);
        }
    }

    @Override
    public List<Course> findCoursesByAccount(Long accountId) {
        Query query = em.createQuery("select c from Course c where c.creator.id=?1");
        query.setParameter(1, accountId);
        return query.getResultList();
    }
}
