package com.soma.rest.resource.asm;

import com.soma.core.model.entities.Account;
import com.soma.rest.mvc.AccountController;
import com.soma.rest.resource.AccountResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by user on 8/21/2015.
 */
public class AccountResourceAsm extends ResourceAssemblerSupport<Account, AccountResource>
{
    public AccountResourceAsm()
    {
        super(AccountController.class, AccountResource.class);
    }

    @Override
    public AccountResource toResource(Account account)
    {
        AccountResource res = new AccountResource();
        res.setName(account.getName());
        res.setPassword(account.getPassword());
        res.setEmail(account.getEmail());
        res.setRid(account.getId());
        res.add(linkTo(methodOn(AccountController.class).getAccount(account.getId())).withSelfRel());
        res.add(linkTo(methodOn(AccountController.class).findAllCourses(account.getId())).withRel("courses"));
        return res;
    }

}
