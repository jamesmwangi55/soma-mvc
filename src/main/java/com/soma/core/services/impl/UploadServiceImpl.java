package com.soma.core.services.impl;

import com.soma.core.model.entities.Upload;
import com.soma.core.repositories.UploadRepo;
import com.soma.core.services.CourseService;
import com.soma.core.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by user on 8/27/2015.
 */
@Service
@Transactional
public class UploadServiceImpl implements UploadService
{

    @Autowired
    private UploadRepo uploadRepo;

    @Override
    public Upload findUpload(Long id) {
        return uploadRepo.findUpload(id);
    }

    @Override
    public Upload deleteUpload(Long id) {
        return uploadRepo.deleteUpload(id);
    }

    @Override
    public Upload updateUpload(Long id, Upload data) {
        return uploadRepo.updateUpload(id, data);
    }
}
