/**
 * Created by user on 9/2/2015.
 */

angular.module('ngApp', [
    'ngApp.home',
    'ngApp.account',
    'ui.router'
])
    .config(function myAppConfig($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/home');
    })
    .controller('home', function($scope, $http){
        //$http.get("/api/items").success(function(data){
        //    $scope.items = data;
        //    console .log($scope.items);
        //});
        $scope.item = "Hello World"
    });