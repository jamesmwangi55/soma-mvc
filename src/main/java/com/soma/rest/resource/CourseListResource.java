package com.soma.rest.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 8/24/2015.
 */
public class CourseListResource extends ResourceSupport
{
    private List<CourseResource> courses = new ArrayList<CourseResource>();

    public List<CourseResource> getCourses()
    {
        return courses;
    }

    public void setCourses(List<CourseResource> courses)
    {
        this.courses = courses;
    }
}
