package com.soma.rest.resource.asm;

import com.soma.core.model.entities.Course;
import com.soma.rest.mvc.AccountController;
import com.soma.rest.mvc.CourseController;
import com.soma.rest.resource.CourseResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class CourseResourceAsm extends ResourceAssemblerSupport<Course, CourseResource>
{
    public CourseResourceAsm()
    {
        super(CourseController.class, CourseResource.class);
    }

    @Override
    public CourseResource toResource(Course course)
    {
        CourseResource resource = new CourseResource();
        resource.setTitle(course.getTitle());
        resource.add(linkTo(CourseController.class).slash(course.getId()).withSelfRel());
        resource.add(linkTo(CourseController.class).slash(course.getId()).slash("uploads").withRel("uploads"));
        if(course.getCreator() != null)
        {
            resource.add(linkTo(AccountController.class).slash(course.getCreator().getId()).withRel("creator"));
        }

        return resource;
    }

}
