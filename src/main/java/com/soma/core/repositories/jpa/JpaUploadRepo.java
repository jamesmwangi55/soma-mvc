package com.soma.core.repositories.jpa;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.repositories.UploadRepo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
@Repository
public class JpaUploadRepo implements UploadRepo
{

    //@PersistenceContext
    public EntityManager em;


    @Override
    public Upload findUpload(Long id) {
        return em.find(Upload.class, id);
    }

    @Override
    public Upload deleteUpload(Long id) {
        Upload upload = em.find(Upload.class, id);
        em.remove(upload);
        return upload;
    }

    @Override
    public Upload updateUpload(Long id, Upload data)
    {
        Upload upload = em.find(Upload.class, id);
        upload.setTitle(data.getTitle());
        upload.setContent(data.getContent());
        return upload;
    }

    @Override
    public Upload createUpload(Upload data) {
        em.persist(data);
        return data;
    }

    @Override
    public List<Upload> findByCourse(Long courseId) {
        Query query = em.createQuery("select u from Upload u WHERE u.course.id=?1");
        query.setParameter(1, courseId);
        return query.getResultList();
    }
}