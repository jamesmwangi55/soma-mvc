package com.soma.rest.resource.asm;

import com.soma.core.services.util.UploadList;
import com.soma.rest.mvc.CourseController;
import com.soma.rest.mvc.UploadController;
import com.soma.rest.resource.UploadListResource;
import com.soma.rest.resource.UploadResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by user on 8/24/2015.
 */
public class UploadListResourceAsm extends ResourceAssemblerSupport<UploadList, UploadListResource>
{

    public UploadListResourceAsm()
    {
        super(UploadController.class, UploadListResource.class);

    }


    @Override
    public UploadListResource toResource(UploadList uploadList) {
        List<UploadResource> resources = new UploadResourceAsm().toResources(uploadList.getUploads());
        UploadListResource listResource = new UploadListResource();
        listResource.setUploads(resources);
        listResource.add(linkTo(methodOn(CourseController.class).findAllUploads(uploadList.getCourseId())).withSelfRel());
        return listResource;
    }
}
