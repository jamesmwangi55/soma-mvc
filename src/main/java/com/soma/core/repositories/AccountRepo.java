package com.soma.core.repositories;

import com.soma.core.model.entities.Account;
import com.soma.core.model.entities.Course;

import java.util.List;

/**
 * Created by user on 8/26/2015.11
 */
public interface AccountRepo
{
    public List<Account> findAllAccounts();
    public Account findAccount(Long id);
    public Account findAccountByName(String name);
    public Account createAccount(Account data);
}
