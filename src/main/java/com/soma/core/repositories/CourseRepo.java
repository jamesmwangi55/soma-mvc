package com.soma.core.repositories;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.services.util.CourseList;
import com.soma.core.services.util.UploadList;

import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public interface CourseRepo
{

    public Course createCourse(Course data);
    public List<Course> findAllCourses();
    public Course findCourse(Long id);
    public Course findCourseByTitle(String title);
    public List<Course> findCoursesByAccount(Long accountId);

}
