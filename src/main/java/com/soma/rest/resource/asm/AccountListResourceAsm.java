package com.soma.rest.resource.asm;

import com.soma.rest.mvc.AccountController;
import com.soma.core.services.util.AccountList;
import com.soma.rest.resource.AccountListResource;
import com.soma.rest.resource.AccountResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public class AccountListResourceAsm extends ResourceAssemblerSupport<AccountList, AccountListResource>
{
    public AccountListResourceAsm(){
        super(AccountController.class, AccountListResource.class);
    }

    @Override
    public AccountListResource toResource(AccountList accountList)
    {
        List<AccountResource> resList = new AccountResourceAsm().toResources(accountList.getAccounts());
        AccountListResource finalRes = new AccountListResource();
        finalRes.setAccounts(resList);
        return finalRes;
    }
}
