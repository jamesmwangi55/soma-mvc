package com.soma.core.services.exceptions;

/**
 * Created by user on 8/20/2015.
 */
public class CourseNotFoundException extends RuntimeException
{
    public CourseNotFoundException(String message)
    {
        super(message);
    }

    public CourseNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CourseNotFoundException()
    {

    }

}
