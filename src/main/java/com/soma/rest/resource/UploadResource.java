package com.soma.rest.resource;

import com.soma.core.model.entities.Upload;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by user on 8/19/2015.
 */
public class UploadResource extends ResourceSupport
{
    private String title;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Upload toUpload()
    {
        Upload upload = new Upload();
        upload.setTitle(title);
        upload.setContent(content);
        return upload;
    }
}
