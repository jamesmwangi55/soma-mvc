package com.soma.core.services.exceptions;

/**
 * Created by user on 8/20/2015.
 */
public class CourseExistsException extends RuntimeException
{
    public CourseExistsException()
    {

    }

    public CourseExistsException(String message)
    {
        super(message);
    }

    public CourseExistsException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CourseExistsException(Throwable cause)
    {
        super(cause);
    }
}
