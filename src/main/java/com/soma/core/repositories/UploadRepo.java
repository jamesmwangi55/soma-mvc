package com.soma.core.repositories;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;

import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public interface UploadRepo
{
    public Upload findUpload(Long id);
    public Upload deleteUpload(Long id);
    public Upload updateUpload(Long id, Upload data);
    public Upload createUpload(Upload data);
    public List<Upload> findByCourse(Long courseId);

}
