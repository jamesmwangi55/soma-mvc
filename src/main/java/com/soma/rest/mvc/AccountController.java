package com.soma.rest.mvc;

import com.soma.core.model.entities.Account;
import com.soma.core.model.entities.Course;
import com.soma.core.services.AccountService;
import com.soma.core.services.exceptions.AccountDoesNotExistException;
import com.soma.core.services.exceptions.AccountExistsException;
import com.soma.core.services.exceptions.CourseExistsException;
import com.soma.core.services.util.AccountList;
import com.soma.core.services.util.CourseList;
import com.soma.rest.exceptions.BadRequestException;
import com.soma.rest.exceptions.ConflictException;
import com.soma.rest.exceptions.NotFoundException;
import com.soma.rest.resource.AccountListResource;
import com.soma.rest.resource.AccountResource;
import com.soma.rest.resource.CourseListResource;
import com.soma.rest.resource.CourseResource;
import com.soma.rest.resource.asm.AccountListResourceAsm;
import com.soma.rest.resource.asm.AccountResourceAsm;
import com.soma.rest.resource.asm.CourseListResourceAsm;
import com.soma.rest.resource.asm.CourseResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by user on 8/21/2015.
 */
@Controller
@RequestMapping("/rest/accounts")
public class AccountController
{
    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService)
    {
        this.accountService = accountService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<AccountListResource> findAllAccounts(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "password", required = false) String password)
    {
        AccountList list = null;
        if(name == null)
        {
            list = accountService.findAllAccounts();
        } else
        {
            Account account = accountService.findAccountsByName(name);
            list = new AccountList(new ArrayList<Account>());
            if(account!=null)
            {
                if(password!=null)
                {
                    if(account.getPassword().equals(password))
                    {
                        list = new AccountList(Arrays.asList(account));
                    }
                } else
                {
                    list = new AccountList(Arrays.asList(account));
                }
            }
        }
        AccountListResource res = new AccountListResourceAsm().toResource(list);
        return new ResponseEntity<AccountListResource>(res, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AccountResource> createAccount(
            @RequestBody AccountResource sentAccount
    )
    {
        try
        {
            Account createdAccount = accountService.createAccount(sentAccount.toAccount());
            AccountResource res = new AccountResourceAsm().toResource(createdAccount);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(res.getLink("self").getHref()));
            return new ResponseEntity<AccountResource>(res, headers, HttpStatus.CREATED);
        } catch (AccountExistsException exception)
        {
            throw new ConflictException(exception);
        }
    }


    @RequestMapping(value = "/{accountId}",
            method = RequestMethod.GET)
    public ResponseEntity<AccountResource> getAccount(
            @PathVariable Long accountId
    )
    {
        Account account = accountService.findAccount(accountId);

        if(account != null)
        {
            AccountResource res = new AccountResourceAsm().toResource(account);
            return new ResponseEntity<AccountResource>(res, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<AccountResource>(HttpStatus.NOT_FOUND);
        }
    }

   @RequestMapping(value = "/{accountId}/courses",
            method = RequestMethod.POST)
    public ResponseEntity<CourseResource> createCourse(
           @PathVariable Long accountId,
           @RequestBody CourseResource resource
   )
   {

       try {
           Course createdCourse = accountService.createCourse(accountId, resource.toCourse());
           CourseResource createdCourseRes = new CourseResourceAsm().toResource(createdCourse);
           HttpHeaders headers = new HttpHeaders();
           headers.setLocation(URI.create(createdCourseRes.getLink("self").getHref()));
           return new ResponseEntity<CourseResource>(createdCourseRes, headers, HttpStatus.CREATED);

       } catch (AccountDoesNotExistException exception)
       {
           throw new BadRequestException(exception);

       } catch (CourseExistsException exception)
       {
           throw new ConflictException(exception);
       }

    }

    @RequestMapping(value = "/{accountId}/courses",
            method = RequestMethod.GET)
    public ResponseEntity<CourseListResource> findAllCourses(
            @PathVariable Long accountId)
    {
        try{
            CourseList courseList = accountService.findCoursesByAccount(accountId);
            CourseListResource courseListResource = new CourseListResourceAsm().toResource(courseList);
            return new ResponseEntity<CourseListResource>(courseListResource, HttpStatus.OK);
        } catch (AccountDoesNotExistException exception)
        {
            throw new NotFoundException(exception);
        }
    }

}
