package com.soma.core.services;

import com.soma.core.model.entities.Account;
import com.soma.core.model.entities.Course;
import com.soma.core.services.util.AccountList;
import com.soma.core.services.util.CourseList;

/**
 * Created by user on 8/20/2015.
 */
public interface AccountService
{
    public Account findAccount(Long id);
    public Account createAccount(Account data);
    public Course createCourse(Long accountId, Course data);
    public CourseList findCoursesByAccount(Long acccountId);
    public AccountList findAllAccounts();
    public Account findAccountsByName(String name);
}
