package com.soma.core.services.exceptions;

/**
 * Created by user on 8/20/2015.
 */
public class AccountExistsException extends RuntimeException
{
    public AccountExistsException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public AccountExistsException(String message)
    {
        super(message);
    }

    public AccountExistsException()
    {
        super();
    }



}
