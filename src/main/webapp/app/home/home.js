angular.module('ngApp.home', ['ui.router', 'ngApp.account'])
    .config(function($stateProvider){
        $stateProvider.state('home', {
            url: '/home',
            views: {
                'main' :
                {
                    templateUrl: 'home/home.tpl.html',
                    controller: 'HomeCtrl'
                }
            },
            data: {pateTitle: 'Home'}
        })
    })
    .controller('HomeCtrl', function HomeController($scope, sessionService ){
            $scope.isLoggedIn = sessionService.isLoggedIn;
            $scope.logout = sessionService.logout;
    });