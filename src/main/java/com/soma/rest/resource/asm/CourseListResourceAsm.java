package com.soma.rest.resource.asm;

import com.soma.core.services.util.CourseList;
import com.soma.rest.mvc.CourseController;
import com.soma.rest.resource.CourseListResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by user on 8/24/2015.
 */
public class CourseListResourceAsm extends ResourceAssemblerSupport<CourseList, CourseListResource>
{
    public CourseListResourceAsm()
    {
        super(CourseController.class, CourseListResource.class);
    }

    @Override
    public CourseListResource toResource(CourseList courseList)
    {
        CourseListResource res = new CourseListResource();
        res.setCourses(new CourseResourceAsm().toResources(courseList.getCourses()));
        return  res;
    }
}
