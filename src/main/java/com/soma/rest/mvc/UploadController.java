package com.soma.rest.mvc;

import com.soma.core.model.entities.Upload;
import com.soma.core.services.UploadService;
import com.soma.rest.resource.UploadResource;
import com.soma.rest.resource.asm.UploadResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/rest/uploads")
public class UploadController
{
	private UploadService uploadService;

	@Autowired
	public UploadController(UploadService uploadService)
	{
		this.uploadService = uploadService;
	}

	@RequestMapping(value = "/{uploadId}", method = RequestMethod.GET)
	public ResponseEntity<UploadResource> getUpload(
			@PathVariable Long uploadId
	)
	{
		Upload upload = uploadService.findUpload(uploadId);

		if(upload != null)
		{
			UploadResource uploadResource = new UploadResourceAsm().toResource(upload);
			return new ResponseEntity<UploadResource>(uploadResource, HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<UploadResource>(HttpStatus.NOT_FOUND);
		}

	}


	@RequestMapping(value = "/{uploadId}",
			method = RequestMethod.DELETE)
	public ResponseEntity<UploadResource> deleteUpload(
			@PathVariable Long uploadId
	){
		Upload upload = uploadService.deleteUpload(uploadId);
		if(upload != null)
		{
			UploadResource res = new UploadResourceAsm().toResource(upload);
			return new ResponseEntity<UploadResource>(res, HttpStatus.OK);
		} else
		{
			return new ResponseEntity<UploadResource>(HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/{uploadId}",
			method = RequestMethod.PUT)
	public ResponseEntity<UploadResource> updateUpload(
			@PathVariable Long uploadId, @RequestBody UploadResource sentUpload
	){
		Upload upload  = uploadService.updateUpload(uploadId, sentUpload.toUpload());
		if(upload != null)
		{
			UploadResource res = new UploadResourceAsm().toResource(upload);
			return new ResponseEntity<UploadResource>(res, HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<UploadResource>(HttpStatus.NOT_FOUND);
		}
	}
}