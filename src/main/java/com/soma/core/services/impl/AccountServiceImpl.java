package com.soma.core.services.impl;

import com.soma.core.model.entities.Account;
import com.soma.core.model.entities.Course;
import com.soma.core.repositories.AccountRepo;
import com.soma.core.repositories.CourseRepo;
import com.soma.core.services.AccountService;
import com.soma.core.services.exceptions.AccountDoesNotExistException;
import com.soma.core.services.exceptions.AccountExistsException;
import com.soma.core.services.exceptions.CourseExistsException;
import com.soma.core.services.util.AccountList;
import com.soma.core.services.util.CourseList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by user on 8/27/2015.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService
{
    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private CourseRepo courseRepo;

    @Override
    public Account findAccount(Long id) {
        return accountRepo.findAccount(id);
    }

    @Override
    public Account createAccount(Account data)
    {
        Account account = accountRepo.findAccountByName(data.getName());

        if(account != null)
        {
            throw new AccountExistsException();
        }
        return accountRepo.createAccount(data);
    }

    @Override
    public Course createCourse(Long accountId, Course data)
    {
        Course courseSameTitle = courseRepo.findCourseByTitle(data.getTitle());

        if(courseSameTitle != null)
        {
            throw new CourseExistsException();
        }

        Account account = accountRepo.findAccount(accountId);
        if(account == null)
        {
            throw new AccountDoesNotExistException();
        }

        Course createdCourse = courseRepo.createCourse(data);
        createdCourse.setCreator(account);
        return createdCourse;
    }

    @Override
    public CourseList findCoursesByAccount(Long acccountId) {
        Account account = accountRepo.findAccount(acccountId);
        if(account == null)
        {
            throw new AccountDoesNotExistException();
        }

        return new CourseList(courseRepo.findCoursesByAccount(acccountId));
    }

    @Override
    public AccountList findAllAccounts() {
        return  new AccountList(accountRepo.findAllAccounts());
    }

    @Override
    public Account findAccountsByName(String name) {
        return accountRepo.findAccountByName(name);
    }
}
