package com.soma.core.services;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.services.util.CourseList;
import com.soma.core.services.util.UploadList;

/**
 * Created by user on 8/20/2015.
 */
public interface CourseService
{
    public Upload createUpload(Long courseId, Upload data);

    public CourseList findAllCourses();

    public UploadList findAllUploads(Long courseId);

    public Course findCourse(Long id);
}
