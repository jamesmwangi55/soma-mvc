package com.soma.core.services.impl;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.repositories.CourseRepo;
import com.soma.core.repositories.UploadRepo;
import com.soma.core.services.CourseService;
import com.soma.core.services.exceptions.CourseNotFoundException;
import com.soma.core.services.util.CourseList;
import com.soma.core.services.util.UploadList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by user on 8/27/2015.
 */
@Service
@Transactional
public class CourseServiceImpl implements CourseService
{

    @Autowired
    private CourseRepo courseRepo;

    @Autowired
    private UploadRepo uploadRepo;

    @Override
    public Upload createUpload(Long courseId, Upload data) {
        Course course = courseRepo.findCourse(courseId);

        if(course == null)
        {
            throw new CourseNotFoundException();
        }

        Upload upload  = uploadRepo.createUpload(data);
        upload.setCourse(course);
        return upload;
    }

    @Override
    public CourseList findAllCourses() {
        return new CourseList(courseRepo.findAllCourses());
    }

    @Override
    public UploadList findAllUploads(Long courseId)
    {
        Course course  = courseRepo.findCourse(courseId);

        if(course == null)
        {
            throw new CourseNotFoundException();
        }

        return new UploadList(courseId, uploadRepo.findByCourse(courseId));
    }

    @Override
    public Course findCourse(Long id) {
        return courseRepo.findCourse(id);
    }
}
