package com.soma.rest.resource.asm;

import com.soma.core.model.entities.Upload;
import com.soma.rest.mvc.CourseController;
import com.soma.rest.mvc.UploadController;
import com.soma.rest.resource.UploadResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by user on 8/19/2015.
 */
public class UploadResourceAsm extends ResourceAssemblerSupport<Upload, UploadResource>
{
    public UploadResourceAsm()
    {
        super(UploadController.class, UploadResource.class);
    }

    @Override
    public UploadResource toResource(Upload upload)
    {
        UploadResource uploadResource = new UploadResource();
        uploadResource.setTitle(upload.getTitle());
        Link link = linkTo(UploadController.class).slash(upload.getId()).withSelfRel();
        uploadResource.add(link.withSelfRel());

        if(upload.getCourse() != null)
        {
            uploadResource.add((linkTo(CourseController.class).slash(upload.getCourse().getId()).withRel("course")));
        }

        return uploadResource;
    }
}
