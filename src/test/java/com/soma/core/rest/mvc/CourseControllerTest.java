package com.soma.core.rest.mvc;

import com.soma.core.model.entities.Account;
import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.services.CourseService;
import com.soma.core.services.exceptions.CourseNotFoundException;
import com.soma.core.services.util.CourseList;
import com.soma.core.services.util.UploadList;
import com.soma.rest.mvc.CourseController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by user on 8/24/2015.
 */
public class CourseControllerTest
{
    @InjectMocks
    private CourseController controller;

    @Mock
    private CourseService courseService;

    private MockMvc mockMvc;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void findAllCourses() throws Exception{
        List<Course> list = new ArrayList<Course>();

        Course courseA = new Course();
        courseA.setId(1L);
        courseA.setTitle("Title A");
        list.add(courseA);


        Course courseB = new Course();
        courseB.setId(1L);
        courseB.setTitle("Title B");
        list.add(courseB);

        CourseList allCourses = new CourseList(list);
        allCourses.setCourses(list);

        when(courseService.findAllCourses()).thenReturn(allCourses);

        mockMvc.perform(get("/rest/courses"))
                .andExpect(jsonPath("$.courses[*].title",
                        hasItems(endsWith("Title A"), endsWith("Title B"))))
                .andExpect(status().isOk());

    }

    @Test
    public void getCourse() throws Exception
    {
        Course course = new Course();
        course.setTitle("Test Title");
        course.setId(1L);

        Account account = new Account();
        account.setId(1L);
        course.setCreator(account);

        when(courseService.findCourse(1L)).thenReturn(course);

        mockMvc.perform(get("/rest/courses/1"))
                .andExpect(jsonPath("$.links[*].href",
                        hasItem(endsWith("/courses/1"))))
                .andExpect(jsonPath("$.links[*].href",
                        hasItem(endsWith("/courses/1/uploads"))))
                .andExpect(jsonPath("$.links[*].href",
                        hasItem(endsWith("/accounts/1"))))
                .andExpect(jsonPath("$.links[*].rel",
                        hasItems(is("self"), is("creator"), is("uploads"))))
                .andExpect(jsonPath("$.title", is("Test Title")))
                .andExpect(status().isOk());
    }

    @Test
    public void createUploadExistitingCourse() throws Exception
    {
        Course course = new Course();
        course.setId(1L);

        Upload upload = new Upload();
        upload.setTitle("Test Title");
        upload.setId(1L);

        when(courseService.createUpload(eq(1L), any(Upload.class))).thenReturn(upload);

        mockMvc.perform(post("/rest/courses/1/uploads")
                .content("{\"title\":\"Generic Title\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", is(upload.getTitle())))
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("rest/uploads/1"))))
                .andExpect(header().string("Location", endsWith("rest/uploads/1")))
                .andExpect(status().isCreated());


    }

    @Test
    public void createUploadNonExistingCourse() throws Exception
    {
        when(courseService.createUpload(eq(1L), any(Upload.class))).thenThrow(new CourseNotFoundException());

        mockMvc.perform(post("/rest/courses/1/uploads")
                .content("{\"title\":\"Generic Title\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void listUploadsForExistingCourses() throws Exception
    {
        Upload uploadA = new Upload();
        uploadA.setId(1L);
        uploadA.setTitle("Test Title");


        Upload uploadB = new Upload();
        uploadB.setId(1L);
        uploadB.setTitle("Test Title");

        List<Upload> uploadsListings = new ArrayList<Upload>();
        uploadsListings.add(uploadA);
        uploadsListings.add(uploadB);

        UploadList list = new UploadList(1L, uploadsListings);
        list.setUploads(uploadsListings);
        list.setCourseId(1L);

        when(courseService.findAllUploads(1L)).thenReturn(list);

        mockMvc.perform(get("/rest/courses/1/uploads"))
                .andDo(print())
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("courses/1/uploads"))))
                .andExpect(jsonPath("$.uploads[*].title", hasItem(is("Test Title"))))
                .andExpect(status().isOk());


    }

    @Test
    public void listUploadsForNonExistingCourse() throws Exception
    {
        when(courseService.findAllUploads(1L)).thenThrow(new CourseNotFoundException());

        mockMvc.perform(get("/rest/courses/1/uploads"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}
