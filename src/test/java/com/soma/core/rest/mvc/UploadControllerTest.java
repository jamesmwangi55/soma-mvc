package com.soma.core.rest.mvc;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.services.UploadService;
import com.soma.rest.mvc.UploadController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.any;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by user on 8/18/2015.
 */
public class UploadControllerTest
{
    @InjectMocks
    private UploadController controller;

    private MockMvc mockMvc;

    @Mock
    private UploadService uploadService;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getExistingFileUpload() throws Exception
    {
        Upload upload = new Upload();
        upload.setId(1L);
        upload.setTitle("Test Title");

        Course course = new Course();
        course.setId(1L);

        upload.setCourse(course);


        when(this.uploadService.findUpload(1L)).thenReturn(upload);

        mockMvc.perform(get("/rest/uploads/1"))
                .andDo(print())
                .andExpect(jsonPath("$.title", is(upload.getTitle())))
                .andExpect(jsonPath("$.links[*].href",
                        hasItems(endsWith("/courses/1"), endsWith("uploads/1"))))
                .andExpect(jsonPath("$.links[*].rel",
                        hasItems(is("self"), is("course"))))
                .andExpect(status().isOk());
    }

    @Test
    public void getNonExistingUpload() throws Exception
    {

        when(uploadService.findUpload(1L)).thenReturn(null);

        mockMvc.perform(get("/rest/uploads/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteExistingUpload() throws Exception
    {
        Upload deletedUpload = new Upload();
        deletedUpload.setId(1L);
        deletedUpload.setTitle("Test Title");

        when(uploadService.deleteUpload(1L)).thenReturn(deletedUpload);

        mockMvc.perform(delete("/rest/uploads/1"))
                .andExpect(jsonPath("$.title", is(deletedUpload.getTitle())))
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/uploads/1"))))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteNonExistingUpload() throws Exception
    {
        when(uploadService.deleteUpload(1L)).thenReturn(null);
        mockMvc.perform(delete("/rest/uploads/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateExistingUpload() throws Exception
    {
        Upload updatedUpload = new Upload();
        updatedUpload.setId(1L);
        updatedUpload.setTitle("Test Title");

        when(uploadService.updateUpload(eq(1L), any(Upload.class))).thenReturn(updatedUpload);

        mockMvc.perform(put("/rest/uploads/1")
                .content("{\"title\":\"Test Title\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", is(updatedUpload.getTitle())))
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("/uploads/1"))))
                .andExpect(status().isOk());
    }

    @Test
    public void updateNonExistingBlogEntry() throws Exception {
        when(uploadService.updateUpload(eq(1L), any(Upload.class)))
                .thenReturn(null);

        mockMvc.perform(put("/rest/uploads/1")
                .content("{\"title\":\"Test Title\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
