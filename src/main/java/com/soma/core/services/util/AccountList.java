package com.soma.core.services.util;

import com.soma.core.model.entities.Account;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 8/27/2015.
 */
public class AccountList
{
    private List<Account> accounts = new ArrayList<Account>();

    public AccountList(List<Account> list)
    {
        this.accounts = list;
    }


    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
