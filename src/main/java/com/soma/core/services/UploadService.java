package com.soma.core.services;

import com.soma.core.model.entities.Upload;

/**
 * Created by user on 8/19/2015.
 */
public interface UploadService
{
    public Upload findUpload(Long id);
    public Upload deleteUpload(Long id);
    public Upload updateUpload(Long id, Upload data);

}
