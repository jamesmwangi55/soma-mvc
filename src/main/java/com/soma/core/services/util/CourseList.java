package com.soma.core.services.util;

import com.soma.core.model.entities.Course;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 8/20/2015.
 */
public class CourseList
{
    private List<Course> courses = new ArrayList<Course>();

    public CourseList(List resultList)
    {
        this.courses = resultList;
    }


    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
