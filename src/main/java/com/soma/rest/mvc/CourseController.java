package com.soma.rest.mvc;

import com.soma.core.model.entities.Course;
import com.soma.core.model.entities.Upload;
import com.soma.core.services.CourseService;
import com.soma.core.services.exceptions.CourseNotFoundException;
import com.soma.core.services.util.CourseList;
import com.soma.core.services.util.UploadList;
import com.soma.rest.exceptions.NotFoundException;
import com.soma.rest.resource.CourseListResource;
import com.soma.rest.resource.CourseResource;
import com.soma.rest.resource.UploadListResource;
import com.soma.rest.resource.UploadResource;
import com.soma.rest.resource.asm.CourseListResourceAsm;
import com.soma.rest.resource.asm.CourseResourceAsm;
import com.soma.rest.resource.asm.UploadListResourceAsm;
import com.soma.rest.resource.asm.UploadResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;

/**
 * Created by user on 8/21/2015.
 */
@Controller
@RequestMapping("/rest/courses")
public class CourseController
{
    public CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService)
    {
        this.courseService = courseService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CourseListResource> findAllCourses()
    {
        CourseList courseList = courseService.findAllCourses();
        CourseListResource courseListResource = new CourseListResourceAsm().toResource(courseList);
        return new ResponseEntity<CourseListResource>(courseListResource, HttpStatus.OK);
    }



    @RequestMapping(value = "/{courseId}",
            method = RequestMethod.GET)
    public ResponseEntity<CourseResource> getCourse(
            @PathVariable Long courseId
    )
    {
        Course course = courseService.findCourse(courseId);
        CourseResource courseResource = new CourseResourceAsm().toResource(course);
        return new ResponseEntity<CourseResource>(courseResource, HttpStatus.OK);
    }

    @RequestMapping(value = "/{courseId}/uploads",
        method = RequestMethod.POST)
    public ResponseEntity<UploadResource> createUpload(
            @PathVariable Long courseId,
            @RequestBody UploadResource sentUpload
    ){
        Upload createdUpload = null;
        try{
            createdUpload = courseService.createUpload(courseId, sentUpload.toUpload());
            UploadResource createdResource = new UploadResourceAsm().toResource(createdUpload);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(createdResource.getLink("self").getHref()));
            return new ResponseEntity<UploadResource>(createdResource, headers, HttpStatus.CREATED);
        }
        catch (CourseNotFoundException e)
        {
            throw new NotFoundException(e);
        }
    }

    @RequestMapping(value = "/{courseId}/uploads",
        method = RequestMethod.GET)
    public ResponseEntity<UploadListResource> findAllUploads(
            @PathVariable Long courseId
    )
    {
        try{
            UploadList list = courseService.findAllUploads(courseId);
            UploadListResource res = new UploadListResourceAsm().toResource(list);
            return new ResponseEntity<UploadListResource>(res, HttpStatus.OK);
        }
        catch (CourseNotFoundException exception)
        {
            throw  new NotFoundException(exception);
        }
    }

}

