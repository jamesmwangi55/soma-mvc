package com.soma.rest.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

/**
 * Created by user on 8/24/2015.
 */
public class UploadListResource extends ResourceSupport
{
    private String title;

    private List<UploadResource> uploads;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<UploadResource> getUploads() {
        return uploads;
    }

    public void setUploads(List<UploadResource> uploads) {
        this.uploads = uploads;
    }
}
