package com.soma.rest.resource;

import com.soma.core.model.entities.Course;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by user on 8/21/2015.
 */
public class CourseResource extends ResourceSupport
{
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Course toCourse()
    {
        Course course = new Course();
        course.setTitle(title);
        return course;
    }

}
