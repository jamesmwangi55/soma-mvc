package com.soma.rest.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.core.model.entities.Account;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by user on 8/21/2015.
 */
public class AccountResource extends ResourceSupport
{
    private String name;
    private String password;
    private String email;
    private Long rid;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @JsonIgnore
    public String getPassword()
    {
        return password;
    }

    @JsonProperty
    public void setPassword(String password)
    {
        this.password = password;
    }

    public Account toAccount()
    {
        Account account = new Account();
        account.setName(name);
        account.setPassword(password);
        account.setEmail(email);
        return account;
    }


}
