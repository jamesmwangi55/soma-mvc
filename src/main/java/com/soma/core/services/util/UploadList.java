package com.soma.core.services.util;

import com.soma.core.model.entities.Upload;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 8/20/2015.
 */
public class UploadList
{
    private List<Upload> uploads = new ArrayList<Upload>();
    private Long courseId;

    public List<Upload> getUploads() {
        return uploads;
    }

    public UploadList(Long courseId, List<Upload> uploads) {
        this.uploads = uploads;
        this.courseId = courseId;
    }

    public void setUploads( List<Upload> uploads) {
        this.uploads = uploads;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }
}
